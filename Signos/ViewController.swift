//
//  ViewController.swift
//  Signos
//
//  Created by Wagner Rodrigues on 22/08/17.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var signos: [String] = []
    var signosSignificado: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        signos.append("Aries")
        signos.append("Touro")
        signos.append("Gemeos")
        signos.append("Cancer")
        signos.append("Leão")
        signos.append("Virgem")
        signos.append("Libra")
        signos.append("Escorpião")
        signos.append("Sargitário")
        signos.append("Capricórnio")
        signos.append("Aquario")
        signos.append("Peixes")
        
        signosSignificado.append("Áries é o primeiro signo astrológico do zodíaco, situado entre Peixes e Touro e associado à constelação de Aries. Seu símbolo é um carneiro.Forma com Leão e Sagitário a triplicidade dos signos do Fogo. É também um dos quatro signos cardinais, juntamente com Câncer, Libra e Capricórnio.            Com pequenas variações nas datas dependendo do ano, os arianos são as pessoas nascidas entre 21 de Março e 20 de Abril.")
        signosSignificado.append("Segundo signo astrológico do zodíaco, situado entre Áries e Gêmeos e associado à constelação de Taurus. Seu símbolo é um boi. Forma com Virgem e Capricórnio a triplicidade dos signos da Terra. É também um dos quatro signos fixos, juntamente com Leão, Escorpião e Aquário. Com pequenas variações nas datas dependendo do ano, os taurinos são as pessoas nascidas entre 21 de Abril e 20 de Maio.")
        signosSignificado.append("Terceiro signo astrológico do zodíaco, situado entre Touro e Câncer e associado à constelação de Gemini.Seu símbolo é os irmãos gêmeos. Forma com Libra e Aquário a triplicidade dos signos do Ar. É também um dos quatro signos mutáveis, juntamente com Virgem, Sagitário e Peixes. Com pequenas variações nas datas dependendo do ano, os geminianos são as pessoas nascidas entre 21 de Maio e 20 de Junho.")
        signosSignificado.append("Quarto signo astrológico do zodíaco, situado entre Gêmeos e Leão e associado à constelação de Cancer. Seu símbolo é um caranguejo. Forma com Escorpião e Peixes a triplicidade dos signos da Água. É também um dos quatro signos cardinais, juntamente com Áries, Libra e Capricórnio. Com pequenas variações nas datas dependendo do ano, os cancerianos são as pessoas nascidas entre 21 de Junho e 21 de Julho.")
        signosSignificado.append("Quinto signo astrológico do zodíaco, situado entre Câncer e Virgem e associado à constelação de Leo.Seu símbolo é um leão. Forma com Áries e Sagitário a triplicidade dos signos do Fogo. É também um dos quatro signos fixos, juntamente com Touro, Escorpião e Aquário. Com pequenas variações nas datas dependendo do ano, os leoninos são as pessoas nascidas entre 22 de Julho e 22 de Agosto.")
        signosSignificado.append("Sexto signo astrológico do zodíaco, situado entre Leão e Libra e associado à constelação de Virgo. Seu símbolo é uma virgem. Forma com Touro e Capricórnio a triplicidade dos signos da Terra. É também um dos quatro signos mutáveis, juntamente com Gêmeos, Sagitário e Peixes. Com pequenas variações nas datas dependendo do ano, os virginianos são as pessoas nascidas entre 23 de agosto e 22 de setembro.")
        signosSignificado.append("Sétimo signo astrológico do zodíaco, situado entre Virgem e Escorpião e associado à constelação de Libra. Seu símbolo é uma balança. Forma com Gêmeos e Aquário a triplicidade dos signos do Ar. É também um dos quatro signos cardinais, juntamente com Áries, Câncer e Capricórnio.Com pequenas variações nas datas dependendo do ano, os librianos são as pessoas nascidas entre 23 de setembro e 22 de outubro.")
        signosSignificado.append("Oitavo signo astrológico do zodíaco, situado entre Libra e Sagitário e associado à constelação de Scorpius.Seu símbolo é um escorpião. Forma com Câncer e Peixes a triplicidade dos signos da Água. É também um dos quatro signos fixos, juntamente com Touro, Leão e Aquário. Com pequenas variações nas datas dependendo do ano, os escorpianos são as pessoas nascidas entre 23 de outubro e 21 de novembro.")
        signosSignificado.append("Nono signo astrológico do zodíaco, situado entre Escorpião e Capricórnio e associado à constelação de Sagittarius. Seu símbolo é o centauro. Forma com Áries e Leão a triplicidade dos signos do Fogo. É também um dos quatro signos mutáveis, juntamente com Gêmeos, Virgem e Peixes. Com pequenas variações nas datas dependendo do ano, os sagitarianos são as pessoas nascidas entre 22 de novembro e 21 de dezembro.")
        signosSignificado.append("Décimo signo astrológico do zodíaco, situado entre Sagitário e Aquário e associado à constelação de Capricornus.Seu símbolo é uma cabra.Forma com Touro e Virgem a triplicidade dos signos da Terra. É também um dos quatro signos cardinais, juntamente com Áries, Câncer e Libra. Com pequenas variações nas datas dependendo do ano, os capricornianos são as pessoas nascidas entre 22 de dezembro e 20 de janeiro.")
        signosSignificado.append("Décimo primeiro signo astrológico do zodíaco, situado entre Capricórnio e Peixes e associado à constelação de Aquarius. Seu símbolo é o aguadouro. Forma com Gêmeos e Libra a triplicidade dos signos do Ar. É também um dos quatro signos fixos, juntamente com Touro, Leão e Escorpião. Com pequenas variações nas datas dependendo do ano, os aquarianos são as pessoas nascidas entre 21 de janeiro e 19 de fevereiro.")
        signosSignificado.append("Décimo segundo signo astrológico do zodíaco, situado entre Aquário e Áries e associado à constelação de Pisces. Seu símbolo é dois peixes. Forma com Câncer e Escorpião a triplicidade dos signos da Água. É também um dos quatro signos mutáveis, juntamente com Gêmeos, Virgem e Sagitário. Com pequenas variações nas datas dependendo do ano, os piscianos são as pessoas nascidas entre 20 de fevereiro e 20 de março.")
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return signos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celularReuso = "celularReuso"
        let celula = tableView.dequeueReusableCell(withIdentifier: celularReuso, for: indexPath)
        celula.textLabel?.text = signos[indexPath.row]
        
        return celula
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let alertaController = UIAlertController(title: "Significado do Signo", message: signosSignificado [indexPath.row], preferredStyle: .alert)
        let acaoConfirmar = UIAlertAction(title: "ok", style: .default, handler: nil)
        
        alertaController.addAction(acaoConfirmar)
        
        present(alertaController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

